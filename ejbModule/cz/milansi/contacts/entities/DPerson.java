package cz.milansi.contacts.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.NamedQueries;
import javax.persistence.Table;

import cz.milansi.contacts.dto.DTOPerson;


/**
 * Objekt reprezntujici tabulku "person" z databaze
 * @author msiebenb
 */
@Entity
@NamedQueries({
	@NamedQuery(name="DPerson_findAll", query = "select p from DPerson p"),
@NamedQuery(name="DPerson_findById", query="select p from DPerson p where p.id = :id"),
@NamedQuery(name="DPerson_findByPrijmeni", query="select p from DPerson p where p.prijmeni = :prijmeni"),
@NamedQuery(name="DPerson_findByPrijmeniJmeno", query="select p from DPerson p where p.prijmeni = :prijmeni and p.jmeno = :jmeno"),
})
@Table(name="person")
public class DPerson {

	public DPerson() {
		// TODO Auto-generated constructor stub
	}
	

	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name="jmeno")
	private String jmeno;
	@Column(name="prijmeni")
	private String prijmeni;
	@Column(name="datvzn")
	private Date datvzn;
	
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="person")
	private List<DPersonContact> contacts;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getJmeno() {
		return jmeno;
	}
	public void setJmeno(String jmeno) {
		this.jmeno = jmeno;
	}
	public String getPrijmeni() {
		return prijmeni;
	}
	public void setPrijmeni(String prijmeni) {
		this.prijmeni = prijmeni;
	}
	public Date getDatvzn() {
		return datvzn;
	}
	public void setDatvzn(Date datvzn) {
		this.datvzn = datvzn;
	}
	
	
	public List<DPersonContact> getContacts() {
		return contacts;
	}
	public void setContacts(List<DPersonContact> contacts) {
		this.contacts = contacts;
	}
	/**
	 * Metoda zkonvertuje predavany objekt na dto.
	 * @return zkonvertovany dto
	 */
	
	public DTOPerson convertToDTO() {
		DTOPerson result = new DTOPerson();
		//result.setId(id);
		result.setJmeno(jmeno);
		result.setPrijmeni(prijmeni);
		result.setDatvzn(datvzn);
		return result;
	}

}
