package cz.milansi.contacts.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cz.milansi.contacts.dto.DTOPerson;
import cz.milansi.contacts.dto.DTOPersonContact;

/**
 * Objekt reprezentujici objekt kontaktu cloveka. Budeme jej vazat na DPerson..
 * @author msiebenb
 *
 */


@Entity
@Table(name="person_contact")
public class DPersonContact {

	public DPersonContact() {
		// TODO Auto-generated constructor stub
	}

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name="typ")
	private int typ;
	
	@Column(name="hodnota")
	private String hodnota;
	
	@ManyToOne
	@JoinColumn(name="person_id", referencedColumnName="id")
	private DPerson person;
	
	
	
	/** 
	 * Vraci textovou reprezentaci objektu.
	 */
	@Override
	public String toString() {
		return "DPersonContact (person=" + person + ", typ + " + typ + ", hodnota = " + hodnota;
	}
	
	/**
	 * Metoda zkonvertuje predavany objekt na dto.
	 * @return zkonvertovany dto
	 */
	
	public DTOPersonContact convertToDTO() {
		DTOPersonContact result = new DTOPersonContact();
		result.setId(id);
		result.setTyp(typ);
		result.setHodnota(hodnota);
		result.setPerson(person.convertToDTO());
		return result;
	}

	
}
