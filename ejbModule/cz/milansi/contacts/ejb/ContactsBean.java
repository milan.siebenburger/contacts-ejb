package cz.milansi.contacts.ejb;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.xml.bind.annotation.XmlElement;

import cz.milansi.contacts.dto.DTOPersonContact;
import cz.milansi.contacts.dto.DTOPerson;
import cz.milansi.contacts.entities.DPerson;
import cz.milansi.contacts.entities.DPersonContact;
import cz.milansi.contacts.ifaces.IContactsRemote;

/**
 * Tento koment je jen pro testovaci ucely tvorby branche a rebase a cherrpick
 * @author ctvrty commit
 */
@Stateless
@WebService(serviceName = "ContactsService")
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ContactsBean implements IContactsRemote, IContactsLocal {

	@PersistenceContext(unitName = "MariaPU-contacts")
	protected EntityManager em;

	/** logovani */
	private static final Logger _LOGGER = Logger.getLogger(ContactsBean.class.getName());

	public ContactsBean() {
		// tady nebude zadny System.out
		System.out.println ("tady mame zmeny cislo rebase1");
	}

	/**
	 * Vraci vstupni retezec doplnenny o cas volani
	 */
	public String ping(@WebParam(name = "instr") String instr) {
		if (_LOGGER.isLoggable(Level.FINE)) {
			_LOGGER.fine("> ContactsBean.ping (instr = " + instr + ")");
		}
		
		System.out.println ("pingpongpignpong");

		return "PONG1 " + instr + " .. generated " + Calendar.getInstance().getTime();
	}
	
	public void uselessMethod(String useless) {
		System.out.println("useless");
	}
	
	public void uselessMethod2nd(String useless) {
		System.out.println("useless");
	}

	/**
	 * Metoda vraci list vsech lidi z databaze. Metoda vraci seznam objektu
	 * DTOPerson. XXX Metoda je jen pro testovací účely, není vhodná při větším
	 * počtu dat..
	 */
	@Override
	@WebMethod(operationName = "getPersons")
	public List<DTOPerson> getPersons() {
		if (_LOGGER.isLoggable(Level.FINE)) {
			_LOGGER.fine("> ContactsBean.getPersons()");
		}

		List<?> lPerson = (List<?>) em.createNamedQuery("DPerson_findAll").getResultList();
		List<DTOPerson> lResult = new ArrayList<DTOPerson>(lPerson.size());

		// překodování do DTO
		for (Object oPerson : lPerson) {
			DPerson person = (DPerson) oPerson;
			DTOPerson dtp = person.convertToDTO();
			person.getContacts().forEach(p -> dtp.add(p.convertToDTO()));
			lResult.add(dtp);
		}

		if (_LOGGER.isLoggable(Level.FINE)) {
			_LOGGER.fine("> ContactsBean.getPersons() .. returns " + lResult.size());
		}

		return lResult;
	}

	/**
	 * Metoda nacte detail z databaze dle predaneho id, zkonvertuje jej do DTO a
	 * vrati
	 * 
	 * @param id id hledaneho cloveka
	 * @return nacteny zaznamy
	 * 
	 */
	// TODO osetrit neexistujici zaznam
	@Override
	@WebMethod(operationName = "getPersonDetail")
	public DTOPerson getPersonDetail(int id) {
		if (_LOGGER.isLoggable(Level.FINE)) {
			_LOGGER.fine("> ContactsBean.getPersonDetail (id = " + id + ")");
		}
		DPerson person = (DPerson) em.createNamedQuery("DPerson_findById").setParameter("id", id).getResultList()
				.get(0);

		// konvert to DTO
		DTOPerson result = person.convertToDTO();

		// pridani kontaktu ve forme DTO do dTOPerson
		person.getContacts().forEach(p -> result.add(p.convertToDTO()));

		if (_LOGGER.isLoggable(Level.FINE)) {
			_LOGGER.fine("< ContactsBean.getPersonDetail (" + person.toString() + ")");
		}
		return result;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@WebMethod(operationName = "addPerson")
	public int addPerson(
			@XmlElement(required = true) @WebParam(name = "jmeno") String jmeno,
			@XmlElement(required = true) @WebParam(name = "prijmeni") String prijmeni) {
		if (_LOGGER.isLoggable(Level.FINE)) {
			_LOGGER.fine("> ContactsBean.addPerson (" + jmeno + ", " + prijmeni + ")");
		}

		List<DPerson> list = em.createNamedQuery("DPerson_findByPrijmeniJmeno").setParameter("prijmeni", prijmeni)
				.setParameter("jmeno", jmeno).getResultList();
		if (!list.isEmpty()) {
			throw new RuntimeException("Záznam s těmito údaji již existuje");
		}
		try {
			DPerson person = new DPerson();
			person.setJmeno(jmeno);
			person.setPrijmeni(prijmeni);
			em.persist(person);
			em.flush();
			if (_LOGGER.isLoggable(Level.FINE)) {
				_LOGGER.fine("< ContactsBean.addPerson .. returns " + person.getId());
			}
			return person.getId();
		} catch (Exception ex) {

			_LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			throw new RuntimeException("CHYBA");
		}
	}

	/**
	 * Nacte cloveka i s kontaky.
	 */
	@Override
	@WebMethod(operationName = "getPersonWithContacts")
	public DTOPerson getPersonWithContacts(@WebParam(name = "idPerson") int id) {
		if (_LOGGER.isLoggable(Level.FINE)) {
			_LOGGER.fine("> ContactsBean.getPersonDetail (id = " + id + ")");
		}

		try {
			DPerson person = (DPerson) em.createNamedQuery("DPerson_findById").setParameter("id", id).getResultList()
					.get(0);

			// konvert to DTO
			DTOPerson lPerson = person.convertToDTO();
			List<DPersonContact> kontakty = person.getContacts();
			List<DTOPersonContact> lKontakty = new ArrayList<>(kontakty.size());
			for (DPersonContact kontakt : kontakty) {
				lKontakty.add(kontakt.convertToDTO());
			}
			lPerson.setKontakty(lKontakty);

			if (_LOGGER.isLoggable(Level.FINE)) {
				_LOGGER.fine("< ContactsBean.getPersonDetail (");
				_LOGGER.fine(person.toString());
				kontakty.forEach(c -> _LOGGER.fine(c.toString()));
				_LOGGER.fine(")");
			}
			return lPerson;
		} catch (Exception ex) {
			_LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
			throw new RuntimeException("CHYBA");
		}

	}

	public String toString() {
		return "ContactsBean "  + this.getPersonDetail(1);
	}

}
